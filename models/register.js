const mongoose = require('mongoose');

const registerSchema = mongoose.Schema({

    email:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    confirmPassword:{
        type:String,
        required:true
    }
});

const register = module.exports = mongoose.model('register',registerSchema);